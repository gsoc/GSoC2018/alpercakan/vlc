/*****************************************************************************
 * Controller.h: Controls the application according to the supplied
 * RemoteCommand objects
 *****************************************************************************
 * Copyright (C) 2008-2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include "RemoteCommand.h"
#include "Tools/Singleton.hpp"
#include "Workflow/MainWorkflow.h"
#include <unordered_map>
#include <QJsonArray>
#include "Backend/IInfo.h"
#include <medialibrary/IMedia.h>

class Controller final : public QObject, public Singleton<Controller>
{
    Q_OBJECT
    Q_DISABLE_COPY(Controller)

public:
    QJsonArray getTransitions() const;
    QJsonArray getEffects() const;
    QJsonArray getRefreshedMediaLibrary() const;
    QJsonObject getWorkspace() const;
    QJsonArray getClipLibrary() const;
    QJsonObject getProjectSettings() const;

public slots:
    void onNewCommandReady(const RemoteCommand &);

private slots:
    void onWorkflowLengthChanged(qint64 length);
    void onClipRemoved(const QString &);
    void onClipAdded(const QString &, const Clip &, quint32, bool);

signals:
    void workflowLengthChanged(QJsonObject);
    void commandCompleted(QJsonObject, RemoteCommand::CommandType);

private:
    static QJsonArray serializeEffectsOrTransitionsList(const std::map<std::string, Backend::IInfo*>&);
    static QJsonObject serializeMedium(const medialibrary::MediaPtr);
    static QJsonObject serializeClip(const Clip &);
    static QJsonObject serializeEffectInstance(const QString &, const EffectHelper &);
    static QJsonObject serializeTrack(const Track &, const quint32);
    static QString encodeMediumThumbnail(const medialibrary::MediaPtr);

    Controller();

    void processAddEffect(const QJsonObject &);

    void processAddTransition(const QJsonObject &);
    void processMoveTransition(const QJsonObject &);
    void processRemoveTransition(const QJsonObject &);
    void processAddTransitionBetweenTracks(const QJsonObject &);
    void processMoveTransitionBetweenTracks(const QJsonObject &);

    void processAddClip(const QJsonObject &);
    void processMoveClip(const QJsonObject &);
    void processResizeClip(const QJsonObject &);
    void processRemoveClip(const QJsonObject &);
    void processSplitClip(const QJsonObject &);
    void processLinkClips(const QJsonObject &);
    void processUnlinkClips(const QJsonObject &);

    MainWorkflow* const m_workflow;
    Settings* const m_settings;

    friend Singleton_t::AllowInstantiation;

    const static std::unordered_map<RemoteCommand::CommandType,
                                    void (Controller::*)(const QJsonObject &),
                                    std::hash<int>> fnMap;

    qint64 workflowLength;
};

#endif // CONTROLLER_H

