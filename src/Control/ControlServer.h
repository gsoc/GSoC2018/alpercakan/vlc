/*****************************************************************************
 * ControlServer.h: Acts as an interface between the controller and the client
 *****************************************************************************
 * Copyright (C) 2008-2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifndef CONTROLSERVER_H
#define CONTROLSERVER_H

#include <QObject>
#include <QQueue>
#include <QtWebSockets/QWebSocketServer>
#include <QtWebSockets/QWebSocket>
#include <queue>
#include "Controller.h"
#include "MessageType.h"


class ControlServer final : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ControlServer)

public:
    explicit ControlServer(const Controller *controller,
                           int port,
                           const QString &expectedToken,
                           QHostAddress expectedAddr,
                           const QString &expectedOrigin);

signals:
    void closed();
    void newCommandReady(const RemoteCommand &);

private slots:
    void onNewConnection();
    void onOriginAuthenticationRequired(QWebSocketCorsAuthenticator *);
    void onMsgReceived(QString);

    void onCmdCompleted(QJsonObject, RemoteCommand::CommandType);
    void onWorkflowLengthChanged(QJsonObject);

private:
    static MessageType cmdTypeToResponseMsgType(RemoteCommand::CommandType);

    bool validateMsg(const QJsonDocument &, const MessageType);
    void processCmdMsg(const QJsonObject &, const QDateTime &, int);
    void sendProjectSettings();
    void sendClipLibrary();
    void sendWorkspace();
    void sendEffectsList();
    void sendTransitionsList();
    void sendRefreshedMediaLibrary();
    void tryAuth(const QJsonObject &);

    void sendResponse(const QJsonObject &, MessageType, bool);
    void sendEvent(const QJsonObject &, MessageType, bool);
    void sendMessage(const QJsonObject &, MessageType, bool, bool);

    const Controller *m_controller;

    int m_port;

    const QString m_expectedToken;
    const QHostAddress m_expectedAddr;
    const QString m_expectedOrigin;

    QWebSocketServer *m_wsServer;
    std::unique_ptr<QWebSocket> m_client;

    bool m_authDone;

    void cleanup();

    std::priority_queue<RemoteCommand,
                        std::vector<RemoteCommand>,
                        std::greater<RemoteCommand>> m_cmdHeap;

    std::queue<QString> m_correspondenceIdQueueArr[MessageType::NbMessageType];

    int m_nextIndex;

    void tryDelegate();
};

#endif // CONTROLSERVER_H
