/*****************************************************************************
 * RemoteCommand.h: Respresens a command received from the remote frontend
 *****************************************************************************
 * Copyright (C) 2008-2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifndef REMOTECOMMAND_H
#define REMOTECOMMAND_H

#include <QDateTime>
#include <QJsonObject>

class RemoteCommand {

public:
    enum CommandType {
        ADD_EFFECT,

        ADD_TRANSITION,
        ADD_TRANSITION_BETWEEN_TRACKS,
        MOVE_TRANSITION,
        MOVE_TRANSITION_BETWEEN_TRACKS,
        REMOVE_TRANSITION,

        ADD_CLIP,
        MOVE_CLIP,
        RESIZE_CLIP,
        REMOVE_CLIP,
        SPLIT_CLIP,
        LINK_CLIPS,
        UNLINK_CLIPS,

        UNDEFINED
    };

    CommandType type;
    QJsonObject payload;

    bool operator >(const RemoteCommand &l) const;

private:
    int index;
    QDateTime timestamp;

    RemoteCommand(QDateTime timestamp,
                  int index,
                  int type,
                  QJsonObject payload);

    friend class ControlServer;
};

#endif // REMOTECOMMAND_H
