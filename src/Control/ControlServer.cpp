/*****************************************************************************
 * ControlServer.cpp: Acts as an interface between the controller and the client
 *****************************************************************************
 * Copyright (C) 2008-2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include "ControlServer.h"
#include "ServerListenException.h"
#include <QtWebSockets/QWebSocketServer>
#include <QtWebSockets/QWebSocket>
#include <QJsonDocument>
#include <QWebSocketCorsAuthenticator>

ControlServer::ControlServer(const Controller *controller,
                             int port,
                             const QString &expectedToken,
                             QHostAddress expectedAddr,
                             const QString &expectedOrigin) :
    m_controller(controller),
    m_port(port),
    m_expectedToken(expectedToken),
    m_expectedAddr(expectedAddr),
    m_expectedOrigin(expectedOrigin),
    m_client(),
    m_authDone(false),
    m_wsServer(new QWebSocketServer("", QWebSocketServer::NonSecureMode, this)),
    m_nextIndex(0),
    m_correspondenceIdQueueArr{ }
{
    connect(this, &ControlServer::newCommandReady,
            m_controller, &Controller::onNewCommandReady);

    connect(m_controller, &Controller::workflowLengthChanged,
            this, &ControlServer::onWorkflowLengthChanged);

    connect(m_controller, &Controller::commandCompleted,
            this, &ControlServer::onCmdCompleted);

    if (m_wsServer->listen(QHostAddress::Any, m_port))
    {
        connect(m_wsServer, &QWebSocketServer::originAuthenticationRequired,
                this, &ControlServer::onOriginAuthenticationRequired);

        connect(m_wsServer, &QWebSocketServer::newConnection,
                this, &ControlServer::onNewConnection);
    }
    else
    {
        throw ServerListenException();
    }
}

void ControlServer::onWorkflowLengthChanged(QJsonObject val)
{
    sendEvent(val,
              MessageType::LengthChangedEvent,
              true);
}

void ControlServer::onNewConnection()
{
    auto next = m_wsServer->nextPendingConnection();

    if (m_client ||
        !next->peerAddress().isEqual(m_expectedAddr, QHostAddress::TolerantConversion))
    {
        if (next)
        {
            next->close();
        }

        return;
    }

    m_client.reset(next);

    connect(m_client.get(), &QWebSocket::textMessageReceived,
            this, &ControlServer::onMsgReceived);
    connect(m_client.get(), &QWebSocket::disconnected,
            this, &ControlServer::closed);

    m_wsServer->close();
}

void ControlServer::onOriginAuthenticationRequired(QWebSocketCorsAuthenticator *authenticator)
{
    authenticator->setAllowed(m_expectedOrigin == authenticator->origin());
}

bool ControlServer::validateMsg(const QJsonDocument &msg,
                                const MessageType expectedType)
{
    if (!msg.isObject())
    {
        return false;
    }

    const QJsonObject obj = msg.object();

    return obj.contains(QStringLiteral("timestamp")) &&
           obj.contains(QStringLiteral("correspondenceId")) &&
           obj.contains(QStringLiteral("index")) &&
           obj[QStringLiteral("type")].toInt() == expectedType &&
           obj.contains(QStringLiteral("content"));
}

void ControlServer::onMsgReceived(QString msg)
{
    const QJsonDocument jsonDoc = QJsonDocument::fromJson(msg.toUtf8());
    const QJsonObject jsonObj = jsonDoc.object();
    const MessageType msgType = static_cast<MessageType>(jsonObj[QStringLiteral("type")].toInt());

    if (!validateMsg(jsonDoc, msgType))
    {
        return;
    }

    m_correspondenceIdQueueArr[msgType + 1].push(jsonObj[QStringLiteral("correspondenceId")].toString());

    if (m_authDone)
    {
        switch(msgType)
        {
        case MessageType::EffectsListRequest:
            sendEffectsList();
            break;

        case MessageType::TransitionsListRequest:
            sendTransitionsList();
            break;

        case MessageType::MediaLibraryRefreshRequest:
            sendRefreshedMediaLibrary();
            break;

        case MessageType::WorkspaceRequest:
            sendWorkspace();
            break;

        case MessageType::ClipLibraryRequest:
            sendClipLibrary();
            break;

        case MessageType::ProjectSettingsRequest:
            sendProjectSettings();
            break;

        default:
            processCmdMsg(jsonObj[QStringLiteral("content")].toObject(),
                          QDateTime::fromString(jsonObj[QStringLiteral("timestamp")].toString(), Qt::ISODate),
                          jsonObj[QStringLiteral("index")].toInt(INT_MAX));
            break;
        }
    }
    else if (msgType == MessageType::AuthRequest)
    {
        tryAuth(jsonObj[QStringLiteral("content")].toObject());
    }
}

MessageType ControlServer::cmdTypeToResponseMsgType(RemoteCommand::CommandType cmdType)
{
    switch (cmdType)
    {
        case RemoteCommand::CommandType::ADD_EFFECT:
            return MessageType::AddEffectResponse;

        case RemoteCommand::CommandType::ADD_CLIP:
            return MessageType::AddClipResponse;

        case RemoteCommand::CommandType::REMOVE_CLIP:
            return MessageType::RemoveClipResponse;
    }
}

void ControlServer::onCmdCompleted(QJsonObject response, RemoteCommand::CommandType cmdType)
{
    sendResponse(response,
                 cmdTypeToResponseMsgType(cmdType),
                 true /* TODO should indicated success status */);
}

void ControlServer::sendProjectSettings()
{
    sendResponse(m_controller->getProjectSettings(), MessageType::ProjectSettingsResponse, true);
}

void ControlServer::sendClipLibrary()
{
    QJsonObject content;

    content.insert(QStringLiteral("clipLibrary"), m_controller->getClipLibrary());

    sendResponse(content, MessageType::ClipLibraryResponse, true);
}

void ControlServer::sendWorkspace()
{
    sendResponse(m_controller->getWorkspace(), MessageType::WorkspaceResponse, true);
}

void ControlServer::sendEffectsList()
{
    QJsonObject content;

    content.insert(QStringLiteral("effects"), m_controller->getEffects());

    sendResponse(content, MessageType::EffectsListResponse, true);
}

void ControlServer::sendTransitionsList()
{
    QJsonObject content;

    content.insert(QStringLiteral("transitions"), m_controller->getTransitions());

    sendResponse(content, MessageType::TransitionsListResponse, true);
}

void ControlServer::sendRefreshedMediaLibrary()
{
    QJsonObject content;

    content.insert(QStringLiteral("media"), m_controller->getRefreshedMediaLibrary());

    sendResponse(content, MessageType::MediaLibraryRefreshResponse, true);
}

void ControlServer::processCmdMsg(const QJsonObject &msg,
                                  const QDateTime &timestamp,
                                  int index)
{
    RemoteCommand rc(timestamp,
                     index,
                     msg[QStringLiteral("type")].toInt(RemoteCommand::CommandType::UNDEFINED),
                     msg[QStringLiteral("payload")].toObject());

    m_cmdHeap.push(rc);
    tryDelegate();
}

void ControlServer::tryAuth(const QJsonObject &authContent)
{
    if (authContent[QStringLiteral("token")].toString() == m_expectedToken)
    {
        m_authDone = true;
    }

    sendResponse(QJsonObject(), MessageType::AuthResponse, m_authDone);

    if (!m_authDone)
    {
        m_client->close();
        m_client.reset();
    }
}

void ControlServer::tryDelegate()
{
    const RemoteCommand &first = m_cmdHeap.top();

    emit newCommandReady(first);
    ++m_nextIndex;
    m_cmdHeap.pop();
}

void ControlServer::sendResponse(const QJsonObject &content,
                                 MessageType type,
                                 bool success)
{
    sendMessage(content, type, success, true);
}

void ControlServer::sendEvent(const QJsonObject &content,
                              MessageType type,
                              bool success)
{
    sendMessage(content, type, success, false);
}

void ControlServer::sendMessage(const QJsonObject &content,
                                MessageType type,
                                bool success,
                                bool isCorrespondence)
{
    if (!m_client || !m_client->isValid())
    {
        return;
    }

    QJsonObject response;

    response.insert(QStringLiteral("content"), content);
    response.insert(QStringLiteral("type"), type);
    response.insert(QStringLiteral("success"), success);

    if (isCorrespondence && !m_correspondenceIdQueueArr[type].empty())
    {
        response.insert(QStringLiteral("correspondenceId"), m_correspondenceIdQueueArr[type].front());
        m_correspondenceIdQueueArr[type].pop();
    }

    m_client->sendTextMessage(QString::fromUtf8(QJsonDocument(response).toJson()));
}
