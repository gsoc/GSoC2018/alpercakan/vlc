
/*****************************************************************************
 * Controller.h: Controls the application according to the supplied
 * RemoteCommand objects
 *****************************************************************************
 * Copyright (C) 2008-2018 VideoLAN
 *
 * Authors: Alper Çakan <alpercakan98@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include "Main/Core.h"
#include "Controller.h"
#include "Backend/IBackend.h"
#include "Backend/IInfo.h"
#include "Library/Library.h"
#include "Library/MediaLibraryModel.h"
#include "Settings/Settings.h"
#include "Media/Clip.h"
#include "Media/Media.h"
#include "Workflow/Track.h"
#include <QFile>

const std::unordered_map<RemoteCommand::CommandType,
                         void (Controller::*)(const QJsonObject &),
                         std::hash<int>>
    Controller::fnMap = {
    { RemoteCommand::CommandType::ADD_EFFECT, &Controller::processAddEffect },

    { RemoteCommand::CommandType::ADD_TRANSITION, &Controller::processAddTransition },
    { RemoteCommand::CommandType::MOVE_TRANSITION, &Controller::processMoveTransition },
    { RemoteCommand::CommandType::REMOVE_TRANSITION, &Controller::processRemoveTransition },
    { RemoteCommand::CommandType::ADD_TRANSITION_BETWEEN_TRACKS, &Controller::processAddTransitionBetweenTracks },
    { RemoteCommand::CommandType::MOVE_TRANSITION_BETWEEN_TRACKS, &Controller::processMoveTransitionBetweenTracks },

    { RemoteCommand::CommandType::ADD_CLIP, &Controller::processAddClip },
    { RemoteCommand::CommandType::MOVE_CLIP, &Controller::processMoveClip },
    { RemoteCommand::CommandType::RESIZE_CLIP, &Controller::processResizeClip },
    { RemoteCommand::CommandType::REMOVE_CLIP, &Controller::processRemoveClip },
    { RemoteCommand::CommandType::SPLIT_CLIP, &Controller::processSplitClip },
    { RemoteCommand::CommandType::LINK_CLIPS, &Controller::processLinkClips },
    { RemoteCommand::CommandType::UNLINK_CLIPS, &Controller::processUnlinkClips },
};

Controller::Controller() :
    m_workflow(Core::instance()->workflow()),
    m_settings(Core::instance()->project()->settings()) {

    connect(m_workflow, &MainWorkflow::lengthChanged, this, &Controller::onWorkflowLengthChanged);

    connect(m_workflow, &MainWorkflow::effectAdded,
            this, [this](const QString &clipId,
                     const EffectHelper &newEffect) {
                         emit commandCompleted(serializeEffectInstance(clipId, newEffect),
                                               RemoteCommand::CommandType::ADD_EFFECT); } );

    connect(m_workflow, &MainWorkflow::clipRemoved,
            this, &Controller::onClipRemoved);

    connect(m_workflow, &MainWorkflow::clipInstanceAdded,
            this, &Controller::onClipAdded);
}

void Controller::onWorkflowLengthChanged(qint64 length)
{
    workflowLength = length;

    QJsonObject jsonObj;

    jsonObj[QStringLiteral("length")] = length;

    emit workflowLengthChanged(jsonObj);
}

void Controller::onClipRemoved(const QString &id)
{
    QJsonObject jsonObj;

    jsonObj[QStringLiteral("id")] = id;

    emit commandCompleted(jsonObj, RemoteCommand::CommandType::REMOVE_CLIP);
}

void Controller::onClipAdded(const QString &instanceId, const Clip &clip, quint32 trackId, bool isAudioClip)
{
    static QString siblingInstanceId = "";
    static quint32 siblingTrackId = 0;

    /*
     * We only have one chance to signal completion of the command.
     * However, adding a clip with both audio and video causes the workflow
     * to signal clipAdded twice, where the first signal is for the audio.
     */
    if (clip.media()->hasAudioTracks() &&
        clip.media()->hasVideoTracks() &&
        isAudioClip)
    {
        siblingInstanceId = instanceId;
        siblingTrackId = trackId;
        return;
    }

    QJsonObject jsonObj, clipJsonObj;
    QJsonArray clips;

    clipJsonObj[QStringLiteral("id")] = instanceId;
    clipJsonObj[QStringLiteral("trackId")] = QJsonValue((qint64) trackId);
    clipJsonObj[QStringLiteral("isAudio")] = isAudioClip;
    clips.append(clipJsonObj);

    if (!siblingInstanceId.isEmpty())
    {
        clipJsonObj[QStringLiteral("id")] = siblingInstanceId;
        clipJsonObj[QStringLiteral("trackId")] = QJsonValue((qint64) siblingTrackId);
        clipJsonObj[QStringLiteral("isAudio")] = true;
        clips.append(clipJsonObj);
    }

    jsonObj[QStringLiteral("clips")] = clips;

    emit commandCompleted(jsonObj, RemoteCommand::CommandType::ADD_CLIP);

    siblingInstanceId = "";
}

void Controller::onNewCommandReady(const RemoteCommand &cmd)
{
    auto entry = fnMap.find(cmd.type);

    if (entry == fnMap.end())
    {
        return; // TODO: Handle undefined commands
    }

    (this->*(entry->second))(cmd.payload);
}

void Controller::processAddEffect(const QJsonObject &payload)
{
    m_workflow->addEffect(payload[QStringLiteral("clipId")].toString(),
                          payload[QStringLiteral("effectName")].toString());
}

void Controller::processAddTransition(const QJsonObject &payload)
{
    m_workflow->addTransition(payload[QStringLiteral("id")].toString(),
                              payload[QStringLiteral("begin")].toInt(),
                              payload[QStringLiteral("end")].toInt(),
                              payload[QStringLiteral("trackId")].toInt(),
                              payload[QStringLiteral("end")].toString());
}

void Controller::processMoveTransition(const QJsonObject &payload)
{
    m_workflow->moveTransition(payload[QStringLiteral("id")].toString(),
                               payload[QStringLiteral("begin")].toInt(),
                               payload[QStringLiteral("end")].toInt());
}

void Controller::processRemoveTransition(const QJsonObject &payload)
{
    m_workflow->removeTransition(payload[QStringLiteral("id")].toString());
}

void Controller::processAddTransitionBetweenTracks(const QJsonObject &payload)
{
    m_workflow->addTransitionBetweenTracks(payload[QStringLiteral("id")].toString(),
                                           payload[QStringLiteral("begin")].toInt(),
                                           payload[QStringLiteral("end")].toInt(),
                                           payload[QStringLiteral("trackAId")].toInt(),
                                           payload[QStringLiteral("trackBId")].toInt(),
                                           payload[QStringLiteral("type")].toString());
}

void Controller::processMoveTransitionBetweenTracks(const QJsonObject &payload)
{
    m_workflow->moveTransitionBetweenTracks(payload[QStringLiteral("id")].toString(),
                                            payload[QStringLiteral("trackAId")].toInt(),
                                            payload[QStringLiteral("trackBId")].toInt());
}

void Controller::processAddClip(const QJsonObject &payload)
{
    m_workflow->addClip(payload[QStringLiteral("clipId")].toString(),
                        payload[QStringLiteral("trackId")].toString().toInt(),
                        payload[QStringLiteral("pos")].toInt());
}

void Controller::processMoveClip(const QJsonObject &payload)
{
    m_workflow->moveClip(payload[QStringLiteral("id")].toString(),
                         payload[QStringLiteral("trackId")].toInt(),
                         payload[QStringLiteral("startFrame")].toInt());
}

void Controller::processResizeClip(const QJsonObject &payload)
{
    m_workflow->resizeClip(payload[QStringLiteral("id")].toString(),
                           payload[QStringLiteral("newBegin")].toInt(),
                           payload[QStringLiteral("newEnd")].toInt(),
                           payload[QStringLiteral("newPos")].toInt());
}

void Controller::processRemoveClip(const QJsonObject &payload)
{
    m_workflow->removeClip(payload[QStringLiteral("id")].toString());
}

void Controller::processSplitClip(const QJsonObject &payload)
{
    m_workflow->splitClip(payload[QStringLiteral("id")].toString(),
                          payload[QStringLiteral("newClipPos")].toInt(),
                          payload[QStringLiteral("newClipBegin")].toInt());
}

void Controller::processLinkClips(const QJsonObject &payload)
{
    m_workflow->linkClips(payload[QStringLiteral("idA")].toString(),
                          payload[QStringLiteral("idB")].toString());
}

void Controller::processUnlinkClips(const QJsonObject &payload)
{
    m_workflow->unlinkClips(payload[QStringLiteral("idA")].toString(),
                            payload[QStringLiteral("idB")].toString());
}

QJsonArray Controller::getTransitions() const
{
    return serializeEffectsOrTransitionsList(Backend::instance()->availableTransitions());
}

QJsonArray Controller::getEffects() const
{
    return serializeEffectsOrTransitionsList(Backend::instance()->availableFilters());
}

QJsonArray Controller::serializeEffectsOrTransitionsList(const std::map<std::string, Backend::IInfo*>& list)
{
    QJsonArray array;

    for (const auto p : list)
    {
        auto info = p.second;
        QJsonObject obj;

        obj[QStringLiteral("id")] = QString::fromStdString(info->identifier());
        obj[QStringLiteral("name")] = QString::fromStdString(info->identifier());
        obj[QStringLiteral("displayName")] = QString::fromStdString(info->name());
        obj[QStringLiteral("description")] = QString::fromStdString(info->description());
        obj[QStringLiteral("author")] = QString::fromStdString(info->author());

        array.append(obj);
    }

    return array;
}

QString Controller::encodeMediumThumbnail(const medialibrary::MediaPtr mediumPtr)
{
    QString thumbnailPath = QString::fromStdString(mediumPtr->thumbnail());

    if (thumbnailPath.isEmpty())
    {
        return thumbnailPath;
    }

    QFile thumbnailFile(thumbnailPath);
    if (!thumbnailFile.open(QIODevice::ReadOnly))
    {
        return QStringLiteral("");
    }

    return "data:image/jpeg;base64," + thumbnailFile.readAll().toBase64();
}

QJsonObject Controller::serializeMedium(const medialibrary::MediaPtr mediumPtr)
{
    QJsonObject jsonObj;

    jsonObj[QStringLiteral("id")] = QString::number(mediumPtr->id());
    jsonObj[QStringLiteral("name")] = QString::fromStdString(mediumPtr->title());
    jsonObj[QStringLiteral("thumbnail")] = encodeMediumThumbnail(mediumPtr);
    jsonObj[QStringLiteral("lengthMs")] = mediumPtr->duration();

    return jsonObj;
}

QJsonObject Controller::serializeEffectInstance(const QString &targetId, const EffectHelper &effect)
{
    QJsonObject jsonObj;

    jsonObj[QStringLiteral("id")] = QUuid::createUuid().toString();
    jsonObj[QStringLiteral("targetId")] = targetId;
    jsonObj[QStringLiteral("effectId")] = effect.identifier();
    jsonObj[QStringLiteral("begin")] = QString::number(effect.begin());
    jsonObj[QStringLiteral("end")] = QString::number(effect.end());

    return jsonObj;
}

QJsonObject Controller::serializeTrack(const Track &track, const quint32 id)
{
    QJsonObject jsonObj;

    jsonObj[QStringLiteral("id")] = QString::number(id);
    jsonObj[QStringLiteral("type")] = track.type();

    return jsonObj;
}

QJsonArray Controller::getRefreshedMediaLibrary() const
{
    QJsonArray array;
    auto library = Core::instance()->library();
    auto model = library->model();
    model->refresh();

    for (const auto mediumPtr : model->allMedia())
    {
        array.append(serializeMedium(mediumPtr));
    }

    return array;
}

QJsonObject Controller::getWorkspace() const
{
    Settings* const workspaceSettings = m_settings->getChild(QStringLiteral("Workspace"));

    QJsonObject jsonObj, tracksObj;

    if (workspaceSettings)
    {
        workspaceSettings->saveJsonTo(tracksObj);
        tracksObj = tracksObj[QStringLiteral("tracks")].toObject();
    }

    jsonObj[QStringLiteral("clips")] = tracksObj[QStringLiteral("clips")];

    jsonObj[QStringLiteral("workflowLength")] = workflowLength;

    QJsonArray tracks;

    for (int isAudio = 0; isAudio <= 1; ++isAudio)
    {
        for (quint32 trackId = 0; trackId <= m_workflow->trackCount(); ++trackId)
        {
            auto trackPtr = m_workflow->track(trackId, isAudio);

            if (trackPtr)
            {
                tracks.append(serializeTrack(*trackPtr, trackId));
            }
        }
    }

    jsonObj[QStringLiteral("tracks")] = tracks;

    return jsonObj;
}

QJsonObject Controller::serializeClip(const Clip &clip)
{
    QJsonObject jsonObj;

    jsonObj[QStringLiteral("id")] = clip.uuid().toString();
    jsonObj[QStringLiteral("parentMediumId")] = QString::number(clip.media()->id());
    jsonObj[QStringLiteral("begin")] = clip.begin();
    jsonObj[QStringLiteral("end")] = clip.end();

    return jsonObj;
}

QJsonArray Controller::getClipLibrary() const
{
    QJsonArray array;
    const auto clips = Core::instance()->library()->allClips();

    for (const auto clipPtr : clips)
    {
        array.append(serializeClip(*clipPtr));
    }

    return array;
}

QJsonObject Controller::getProjectSettings() const
{
    QJsonObject jsonObj;

    jsonObj[QStringLiteral("fps")] = QJsonValue::fromVariant(m_settings->value(QStringLiteral("video/VLMCOutputFPS"))->get());

    return jsonObj;
}
